
# -*- coding: utf-8 -*-
"""simple_gcn_baseline.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1mHiChD2uUs0zERNO8f98Ppcl9YJrUpMB
"""

# !pip3 install -q transformers
# !git clone https://bean1337@bitbucket.org/bean1337/reu.git
# !pip3 install git+https://github.com/kunaldahiya/pyxclib.git

# Importing stock ml libraries
import numpy as np
import pandas as pd
from sklearn import metrics
import transformers
import torch
from torch.utils.data import Dataset, DataLoader, RandomSampler, SequentialSampler
from transformers import BertTokenizer, BertModel, BertConfig
from sklearn.preprocessing import MultiLabelBinarizer
from xclib.data import data_utils
import xclib.evaluation.xc_metrics as xc_metrics

import warnings
warnings.filterwarnings('ignore')
import logging

logging.getLogger("transformers").setLevel(logging.WARNING)
from torch import cuda
device = 'cuda' if cuda.is_available() else 'cpu'
import os
import pickle
import tqdm


def set_seed(seed):
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    os.environ['PYTHONHASHSEED'] = str(seed)
    
set_seed(190)

# from google.colab import drive
# drive.mount('/content/drive')
df_train = pd.read_csv('eurlex_train.csv')
df_test  = pd.read_csv('eurlex_test.csv')


import pickle as pk
with open('eurlex_gcn_data.pk','rb') as f:
  da_adj = pk.load(f)


embd  = da_adj['emd']
adj_d = da_adj['adj']

# df_train = pd.read_csv('./drive/MyDrive/eurlex_train.csv') 
# df_test  = pd.read_csv('./drive/MyDrive/eurlex_test.csv')

# Sections of config

# Defining some key variables that will be used later on in the training
MAX_LEN = 512
TRAIN_BATCH_SIZE = 16
VALID_BATCH_SIZE = 8
EPOCHS = 50
LEARNING_RATE = 1e-04
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

import ast
class CustomDataset(Dataset):

    def __init__(self, dataframe, tokenizer, max_len):
        self.tokenizer    = tokenizer
        self.data         = dataframe
        self.comment_text = dataframe.comment_text
        self.targets      = self.data.list
        self.max_len      = max_len

    def __len__(self):
        return len(self.comment_text)

    def __getitem__(self, index):
        comment_text = str(self.comment_text[index])
        comment_text = " ".join(comment_text.split())

        inputs = self.tokenizer.encode_plus(
            comment_text,
            None,
            add_special_tokens=True,
            max_length=self.max_len,
            pad_to_max_length=True,
            return_token_type_ids=True,truncation=True
        )
        ids = inputs['input_ids']
        mask = inputs['attention_mask']
        token_type_ids = inputs["token_type_ids"]


        return {
            'ids': torch.tensor(ids, dtype=torch.long),
            'mask': torch.tensor(mask, dtype=torch.long),
            'token_type_ids': torch.tensor(token_type_ids, dtype=torch.long),
            'targets': torch.tensor(ast.literal_eval(self.targets[index]), dtype=torch.float)
        }





# import ast
# class CustomDataset(Dataset):

#     def __init__(self, dataframe, tokenizer, max_len):
#         self.tokenizer    = tokenizer
#         self.data         = dataframe
#         self.comment_text = dataframe.comment_text
#         self.targets      = self.data.list
#         self.max_len      = max_len
#         self.emd_         = embd

#     def __len__(self):
#         return len(self.comment_text)

#     def __getitem__(self, index):
#         comment_text = str(self.comment_text[index])
#         comment_text = " ".join(comment_text.split())

#         inputs = self.tokenizer.encode_plus(
#             comment_text,
#             None,
#             add_special_tokens=True,
#             max_length=self.max_len,
#             pad_to_max_length=True,
#             return_token_type_ids=True
#         )
#         ids = inputs['input_ids']
#         mask = inputs['attention_mask']
#         token_type_ids = inputs["token_type_ids"]


#         return {
#             'ids': torch.tensor(ids, dtype=torch.long),
#             'mask': torch.tensor(mask, dtype=torch.long),
#             'token_type_ids': torch.tensor(token_type_ids, dtype=torch.long),
#             'targets': torch.tensor(ast.literal_eval(self.targets[index]), dtype=torch.float)
#         }

# print("FULL Dataset: {}".format(df_new.shape))
print("TRAIN Dataset: {}".format(df_train.shape))
print("TEST Dataset: {}".format(df_test.shape))

training_set = CustomDataset(df_train, tokenizer, MAX_LEN)
testing_set = CustomDataset(df_test, tokenizer, MAX_LEN)

train_params = {'batch_size': TRAIN_BATCH_SIZE,
                'shuffle': True,
                'num_workers': 0
                }

test_params = {'batch_size': VALID_BATCH_SIZE,
                'shuffle': True,
                'num_workers': 0
                }

training_loader = DataLoader(training_set, **train_params)
testing_loader = DataLoader(testing_set, **test_params)



from torch.nn import Parameter
import torch.nn as nn
import math

"""
class GraphConvolution(nn.Module):
    """
    Simple GCN layer, similar to https://arxiv.org/abs/1609.02907
    """

    def __init__(self, in_features, out_features, bias=False):
        super(GraphConvolution, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(in_features, out_features))
        if bias:
            self.bias = Parameter(torch.Tensor(1, 1, out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input, adj):
        support = torch.matmul(input, self.weight)
        output = torch.matmul(adj, support)
        if self.bias is not None:
            return output + self.bias
        else:
            return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
               + str(self.in_features) + ' -> ' \
               + str(self.out_features) + ')'



import torch
import torch.nn as nn
import torch.nn.functional as F





class GraphAttentionLayera(nn.Module):
    """
    Simple GAT layer, similar to https://arxiv.org/abs/1710.10903
    """
    def __init__(self, in_features, out_features, dropout, alpha, concat=True):
        super(GraphAttentionLayera, self).__init__()
        self.dropout = dropout
        self.in_features = in_features
        self.out_features = out_features
        self.alpha = alpha
        self.concat = concat

        self.W = nn.Parameter(torch.empty(size=(in_features, out_features)))
        nn.init.xavier_uniform_(self.W.data, gain=1.414)
        self.a = nn.Parameter(torch.empty(size=(2*out_features, 1)))
        nn.init.xavier_uniform_(self.a.data, gain=1.414)

        self.leakyrelu = nn.LeakyReLU(self.alpha)

    def forward(self, h, adj):
        Wh = torch.mm(h, self.W) # h.shape: (N, in_features), Wh.shape: (N, out_features)
        e = self._prepare_attentional_mechanism_input(Wh)

        zero_vec = -9e15*torch.ones_like(e)
        attention = torch.where(adj > 0, e, zero_vec)
        attention = F.softmax(attention, dim=1)
        attention = F.dropout(attention, self.dropout, training=self.training)
        h_prime = torch.matmul(attention, Wh)

        if self.concat:
            return F.elu(h_prime)
        else:
            return h_prime
        
    def _prepare_attentional_mechanism_input(self, Wh):
        # Wh.shape (N, out_feature)
        # self.a.shape (2 * out_feature, 1)
        # Wh1&2.shape (N, 1)
        # e.shape (N, N)
        Wh1 = torch.matmul(Wh, self.a[:self.out_features, :])
        Wh2 = torch.matmul(Wh, self.a[self.out_features:, :])
        # broadcast add
        e = Wh1 + Wh2.T
        return self.leakyrelu(e)

    def __repr__(self):
        return self.__class__.__name__ + ' (' + str(self.in_features) + ' -> ' + str(self.out_features) + 
')'


"""
import pickle

def gen_A(num_classes, t, adj_file):
    import pickle
    result = adj_file
    _adj = result['adj']
    _nums = result['nums']
    _nums = _nums[:, np.newaxis]
    _adj = _adj / _nums
    _adj[_adj < t] = 0
    _adj[_adj >= t] = 1
    _adj = _adj * 0.25 / (_adj.sum(0, keepdims=True) + 1e-6)
    _adj = _adj + np.identity(num_classes, np.int)
    return _adj

def gen_adj(A):
    D = torch.pow(A.sum(1).float(), -0.5)
    D = torch.diag(D)
    adj = torch.matmul(torch.matmul(A, D).t(), D)
    return adj




# class BERTClass(torch.nn.Module):
#     def __init__(self):
#         super(BERTClass, self).__init__()
#         self.l1 = transformers.BertModel.from_pretrained('bert-base-uncased',return_dict=False)
#         self.gc1  = GraphAttentionLayera(768, 1024,0.0,0.2)
#         self.gc2  = GraphAttentionLayera(1024, 768,0.0,0.2)

#         self.relu = nn.LeakyReLU(0.2)
#         self.EMB  = Parameter(torch.from_numpy(embd).float(),requires_grad=True)
#         _adj      = gen_A(50, 0.4, adj_d)
#         self.A    = Parameter(torch.from_numpy(_adj).float(),requires_grad=True)

    
#     def forward(self, ids, mask, token_type_ids):
#         _, output_1= self.l1(ids, attention_mask = mask, token_type_ids = token_type_ids)
#         adj = gen_adj(self.A).detach()
#         x = self.gc1(self.EMB, adj)
#         x = self.relu(x)
#         x = self.gc2(x, adj)

#         x = x.transpose(0, 1)
#         x = torch.matmul(output_1, x)

#         return x


class BERTClass(torch.nn.Module):
    def __init__(self):
        super(BERTClass, self).__init__()
        self.l1 = transformers.BertModel.from_pretrained('bert-base-uncased',return_dict=False)
        self.l2 = torch.nn.Dropout(0.2)
        self.l3 = torch.nn.Linear(768, 50)
    
    def forward(self, ids, mask, token_type_ids):
        _, output_1= self.l1(ids, attention_mask = mask, token_type_ids = token_type_ids)
        output_2 = self.l2(output_1)
        output = self.l3(output_2)
        return output

model = BERTClass()
model.to(device)


model = BERTClass()
model.to(device)

from transformers import get_linear_schedule_with_warmup, AdamW, BertConfig

optimizer = AdamW(model.parameters(),
                  lr = 2e-5, # args.learning_rate - default is 5e-5, our notebook had 2e-5
                  eps = 1e-8 # args.adam_epsilon  - default is 1e-8.
                )
total_steps = len(training_loader) * EPOCHS
scheduler = get_linear_schedule_with_warmup(optimizer, 
                                            num_warmup_steps = 0, # Default value in run_glue.py
                                            num_training_steps = total_steps)

def loss_fn(outputs, targets):
    return torch.nn.BCEWithLogitsLoss()(outputs, targets)




# optimizer = torch.optim.Adam(params =  model.parameters(), lr=LEARNING_RATE)

from tqdm import tqdm
def train(epoch):
    model.train()
    for _,data in enumerate(tqdm(training_loader), 0):
        ids = data['ids'].to(device, dtype = torch.long)
        mask = data['mask'].to(device, dtype = torch.long)
        token_type_ids = data['token_type_ids'].to(device, dtype = torch.long)
        targets = data['targets'].to(device, dtype = torch.float)

        outputs = model(ids, mask, token_type_ids)

        optimizer.zero_grad()
        loss = loss_fn(outputs, targets)
        if _%2000==0:
            print(f'Epoch: {epoch}, Loss:  {loss.item()}')
        
        optimizer.zero_grad()
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
        optimizer.step()
        scheduler.step()

def validation(epoch):
    model.eval()
    fin_targets=[]
    fin_outputs=[]

    hit_list   = []
    num_sample = 0
    results = {}
    with torch.no_grad():
        for _, data in enumerate(tqdm(testing_loader), 0):
            ids = data['ids'].to(device, dtype = torch.long)
            mask = data['mask'].to(device, dtype = torch.long)
            token_type_ids = data['token_type_ids'].to(device, dtype = torch.long)
            targets = data['targets'].to(device, dtype = torch.float)
            outputs = model(ids, mask, token_type_ids)
            fin_targets.extend(targets.cpu().detach().numpy().tolist())
            fin_outputs.extend(torch.sigmoid(outputs).cpu().detach().numpy().tolist())

            batch_list_2, batch_num = eval_batch(outputs, targets)
            hit_list.append(batch_list_2)
            num_sample += batch_num

        p1, p3, p5 = eval_precision(hit_list, num_sample)
        result = {'p1': p1, 'p3': p3, 'p5': p5}
        results.update(result)
        print('p1 : {:.2f}   p3 : {:.2f}    p5 : {:.2f}'.format(p1 * 100, p3 * 100, p5 * 100))

    return fin_outputs, fin_targets, result


def write_result(data):

  with open('result_bert.txt','a') as f:
    f.write(str(data) + '\n')

def eval_precision(hit_list, num_sample):
    num_p1, num_p3, num_p5 = 0, 0, 0
    for item in hit_list:  # k = 1, 3, 5
        num_p1 += item[0]
        num_p3 += item[1]
        num_p5 += item[2]
    p1 = num_p1 / num_sample
    p3 = num_p3 / (num_sample * 3)
    p5 = num_p5 / (num_sample * 5)

    return p1, p3, p5



def eval_batch(logits, truth):
    # row, col = predict.shape
    k = 10
    result  = torch.sigmoid(logits)
    pre_ind = torch.topk(result, k, dim=1)[1]

    pre_ind = pre_ind.detach().cpu().numpy()
    truth   = truth.detach().cpu().numpy()

    row, col = truth.shape

    hit_list = []
    for k in range(1, 6, 2):  # k = 1, 3, 5
        pre_k = pre_ind[:,:k]
        hit_sum = 0
        for i in range(row):

            intersection = np.intersect1d(pre_k[i,:], truth[i,:])
            hit_sum += len(intersection)

        hit_list.append(hit_sum)

    return hit_list, row

def get_result(epoch):
  outputs, targets, result = validation(epoch)
  outputs = np.array(outputs) >= 0.5
  accuracy = metrics.accuracy_score(targets, outputs)
  f1_score_micro = metrics.f1_score(targets, outputs, average='micro')
  f1_score_macro = metrics.f1_score(targets, outputs, average='macro')

  print("TEST AUC-macro : {:.6f}".format(metrics.roc_auc_score(targets, outputs, average='macro')))
  print("TEST AUC-micro : {:.6f}".format(metrics.roc_auc_score(targets, outputs, average='micro')))

  # evaluate (See examples/evaluate.py for more details)
  acc = xc_metrics.Metrics(true_labels=np.array(targets))
  args = acc.eval(np.array(outputs.astype(np.float)), 5)
  print(xc_metrics.format(*args))

  format_result = f"  epoch : {epoch}      xc matrix : {str([xc_metrics.format(*args)])}        Accuracy 
Score : {accuracy}        F1 Score (Micro) : {f1_score_micro}           F1 Score (Macro) : 
{f1_score_macro}           result : {result}"

  try:
    write_result(str(format_result))
  except Exception as e:
    pass
  print(f"Accuracy Score = {accuracy}")
  print(f"F1 Score (Micro) = {f1_score_micro}")
  print(f"F1 Score (Macro) = {f1_score_macro}")

for epoch in range(EPOCHS):
    train(epoch)
    get_result(epoch)


