from transformers import BertTokenizer, BertModel
import torch
import numpy as np
from tqdm import tqdm

import pandas as pd
import pickle as pk
import ast

tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
model     = BertModel.from_pretrained('bert-base-uncased')


class GcnData(object):

  def __init__(self, label_df):

    self.df = label_df

  def get_embeddings(self, word):

    inputs = tokenizer(word, return_tensors='pt')
    outputs = model(**inputs)
    result = outputs.pooler_output
    return result.cpu().detach().numpy()


  def label_emb(self):

    """ GCN feature matrix """

    label_e = []
    label_order = list(self.df.columns)
    for i in tqdm(label_order):
      label_e.append(self.get_embeddings(i).squeeze())
    return np.array(label_e)


  def get_adj_matrix(self):

    """GCN adj matrix
    where df should be one hot of labels"""

    all_categoris = list(self.df.columns)
    label_freq    = {}

    for i in all_categoris:
      try:
        label_freq[i] = self.df[i].value_counts()[1]
      except Exception as e:
         label_freq[i] = 0
    
    u     = np.diag(np.ones(self.df.shape[1], dtype=bool))
    adj_m = self.df.T.dot(self.df) * (~u)
    adj_m = adj_m.to_numpy()

    data = {'adj': adj_m, 'nums': np.array(list(label_freq.values()))}
    return data


def get_gcn_config(data_set_name, label_order_path, df_csv_path):
  with open(label_order_path,'rb') as f:
    la_or = pk.load(f)


  df_train         = pd.read_csv(df_csv_path)
  df_train['list'] = df_train['list'].apply(lambda x: ast.literal_eval(x))
  data             = list(df_train['list'])
  df               = pd.DataFrame.from_records(data, columns=la_or)

  print("generating gcn data")

  gcn_d = GcnData(df)
  embd  = gcn_d.label_emb()
  adj   = gcn_d.get_adj_matrix()

  file_data = {'adj': adj, 'emd': embd}

  with open(f'{data_set_name}_gcn_data.pk','wb') as f:
    pk.dump(file_data,f)

get_gcn_config('eurlex', 'eurlex_labels_order.pk', 'eurlex_train.csv')