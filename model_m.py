import numpy as np


def bert_tokenizer(tokenizer, sent, max_len):
    encoded_dict = tokenizer.encode_plus(text=sent, add_special_tokens=True, max_length=max_len,
                                         pad_to_max_length=True, return_attention_mask=True)
    input_id = encoded_dict['input_ids']
    attention_mask = encoded_dict['attention_mask']
    token_type_id = encoded_dict['token_type_ids']

    return input_id, attention_mask, token_type_id


def model_input(X, y, tokenizer, max_len):
    X_input = []
    X_attention = []
    X_token_type = []

    X_data_labels = []

    for sent, label in zip(X, y):
        input_ = []
        attention_ = []
        token_type = []

        for X_sent in sent:
            input_id, attention_mask, token_type_id = bert_tokenizer(tokenizer, X_sent, max_len)
            input_.append(input_id)
            attention_.append(attention_mask)
            token_type.append(token_type_id)

        X_input.append(input_)
        X_attention.append(attention_)
        X_token_type.append(token_type)

        X_data_labels.append(label)

    X_data_labels = np.asarray(X_data_labels, dtype=np.int32)

    X_input = np.array(X_input, dtype=int)
    X_input = np.reshape(X_input, (X_input.shape[0], X_input.shape[1] * X_input.shape[2]))

    X_attention = np.array(X_attention, dtype=int)
    X_attention = np.reshape(X_attention, (X_attention.shape[0], X_attention.shape[1] * X_attention.shape[2]))

    X_token_type = np.array(X_token_type, dtype=int)
    X_token_type = np.reshape(X_token_type, (X_token_type.shape[0], X_token_type.shape[1] * X_token_type.shape[2]))

    X_inputs = (X_input, X_attention, X_token_type)

    return X_inputs, X_data_labels


import pandas as pd
import ast
df_train = pd.read_csv('eurlex_train.csv')
df_test  = pd.read_csv('eurlex_test.csv')

dfr = pd.concat([df_train,df_test]).reset_index(drop=True)

texts  = list(dfr['comment_text'])
labels = list(dfr['list'].apply(lambda x : ast.literal_eval(x)))




import tensorflow as tf
from tensorflow.python.keras import backend as K
from transformers import *


class Sequence_Attention(tf.keras.layers.Layer):
    def __init__(self, class_num):
        super(Sequence_Attention, self).__init__(class_num)
        self.class_num = class_num
        self.Ws = None

    def build(self, input_shape):
        embedding_length = int(input_shape[2])

        self.Ws = self.add_weight(shape=(self.class_num, embedding_length),
                                  initializer=tf.keras.initializers.get('glorot_uniform'), trainable=True)

        super(Sequence_Attention, self).build(input_shape)

    def call(self, inputs):

        sentence_trans = tf.transpose(inputs, [0, 2, 1])

        at = tf.matmul(self.Ws, sentence_trans)

        at = tf.math.tanh(at)

        at = K.exp(at - K.max(at, axis=-1, keepdims=True))
        at = at / K.sum(at, axis=-1, keepdims=True)

        v = K.batch_dot(at, inputs)

        return v


class Sequence_Attention(tf.keras.layers.Layer):
    def __init__(self, class_num):
        super(Sequence_Attention, self).__init__(class_num)
        self.class_num = class_num
        self.Ws = None

    def build(self, input_shape):
        embedding_length = int(input_shape[2])

        self.Ws = self.add_weight(shape=(self.class_num, embedding_length),
                                  initializer=tf.keras.initializers.get('glorot_uniform'), trainable=True)

        super(Sequence_Attention, self).build(input_shape)

    def call(self, inputs):

        sentence_trans = tf.transpose(inputs, [0, 2, 1])

        at = tf.matmul(self.Ws, sentence_trans)

        at = tf.math.tanh(at)

        at = K.exp(at - K.max(at, axis=-1, keepdims=True))
        at = at / K.sum(at, axis=-1, keepdims=True)

        v = K.batch_dot(at, inputs)

        return v


class D2SBERT_Model(tf.keras.Model):
    def __init__(self, model_name, dir_path, num_class):
        super(D2SBERT_Model, self).__init__()

        self.num_class = num_class

        self.bert = TFBertModel.from_pretrained(model_name, cache_dir=dir_path, output_attentions=True, from_pt=True)

        self.att = Sequence_Attention(self.num_class)

        self.dropout = tf.keras.layers.Dropout(self.bert.config.hidden_dropout_prob)

        self.classifier = [tf.keras.layers.Dense(1, activation='sigmoid',
                                                 kernel_initializer=tf.keras.initializers.TruncatedNormal(
                                                     self.bert.config.initializer_range))
                           for _ in range(self.num_class)]

    def call(self, inputs, attention_mask=None, token_type_ids=None, training=False):

        ids = tf.keras.layers.Reshape((25, 512))(inputs[0])
        mask = tf.keras.layers.Reshape((25, 512))(inputs[1])
        token = tf.keras.layers.Reshape((25, 512))(inputs[2])

        ids = tf.transpose(ids, [1, 0, 2])
        mask = tf.transpose(mask, [1, 0, 2])
        token = tf.transpose(token, [1, 0, 2])

        CLS_tokens = []

        for i in range(ids.shape[0]):
            CLS_tokens.append(self.bert(ids[i], attention_mask=mask[i], token_type_ids=token[i])[1])

        CLS_tokens = tf.stack(CLS_tokens, axis=1)

        CLS_tokens = self.att(CLS_tokens)

        labels = []

        for i in range(self.num_class):
            labels.append(self.classifier[i](self.dropout(CLS_tokens[::, i])))

        labels = tf.stack(labels, axis=1)

        labels = tf.squeeze(labels, [2])

        return labels


import os
import csv
from sklearn.model_selection import train_test_split
from transformers import *
import tensorflow_addons as tfa
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
import tensorflow as tf
from sklearn.metrics import roc_auc_score

os.environ["CUDA_VISIBLE_DEVICES"] = "1, 2"

strategy = tf.distribute.MirroredStrategy(devices=["GPU:0", "GPU:1"])

max_len = 512

code = ['401.9', '38.93', '428.0', '427.31', '414.01', '96.04', '96.6', '584.9', '250.00', '96.71', '272.4', '518.81',
        '99.04', '39.61', '599.0', '530.81', '96.72', '272.0', '285.9', '88.56', '244.9', '486', '38.91', '285.1',
        '36.15', '276.2', '496', '99.15', '995.92', 'V58.61', '507.0', '038.9', '88.72', '585.9', '403.90', '311',
        '305.1', '37.22', '412', '33.24', '39.95', '287.5', '410.71', '276.1', 'V45.81', '424.0', '45.13', 'V15.82',
        '511.9', '37.23']


X_train, X_test, y_train, y_test = train_test_split(texts, labels, test_size=0.2, random_state=42)
X_dev, X_test, y_dev, y_test = train_test_split(X_test, y_test, test_size=0.5, random_state=42)

tokenizer = BertTokenizer.from_pretrained("dmis-lab/biobert-base-cased-v1.1-mnli")

train_inputs, train_data_labels = model_input(X_train, y_train, tokenizer, max_len)
dev_inputs, dev_data_labels = model_input(X_dev, y_dev, tokenizer, max_len)
test_inputs, test_data_labels = model_input(X_test, y_test, tokenizer, max_len)

with strategy.scope():

    cls_model = D2SBERT_Model(model_name='dmis-lab/biobert-base-cased-v1.1-mnli', dir_path='bert_ckpt', num_class=50)
    F1_macro = tfa.metrics.F1Score(num_classes=50, average='macro', threshold=0.5, name='f1_macro')
    F1_micro = tfa.metrics.F1Score(num_classes=50, average='micro', threshold=0.5, name='f1_micro')

    optimizer = tf.keras.optimizers.Adam(1e-5)
    loss = tf.keras.losses.BinaryCrossentropy()
    metric = tf.keras.metrics.BinaryAccuracy()

    cls_model.compile(optimizer=optimizer, loss=loss, metrics=[metric, F1_macro, F1_micro])

EarlyStopping = EarlyStopping(monitor='val_f1_macro', verbose=1, min_delta=0.0001, patience=4, mode='max',
                              restore_best_weights=True)

checkpoint_path = os.path.join('./', '', 'weights.h5')
checkpoint_dir = os.path.dirname(checkpoint_path)

if os.path.exists(checkpoint_dir):
    print("{} -- Folder already exists \n".format(checkpoint_dir))
else:
    os.makedirs(checkpoint_dir, exist_ok=True)
    print("{} -- Folder create complete \n".format(checkpoint_dir))

cp_callback = ModelCheckpoint(
    checkpoint_path, monitor='val_f1_macro', mode='max', verbose=1, save_best_only=True, save_weights_only=True)

cls_model.fit(train_inputs, train_data_labels, epochs=50, batch_size=4, validation_data=(dev_inputs, dev_data_labels),
              callbacks=[EarlyStopping, cp_callback])

test_loss, test_acc, test_macro, test_micro = cls_model.evaluate(test_inputs, test_data_labels, batch_size=4)
test_predict = cls_model.predict(test_inputs)

print("TEST Loss : {:.6f}".format(test_loss))
print("TEST ACC : {:.6f}".format(test_acc))
print("TEST F1-macro : {:.6f}".format(test_macro))
print("TEST F1-micro : {:.6f}".format(test_micro))
print("TEST AUC-macro : {:.6f}".format(roc_auc_score(test_data_labels, test_predict, average='macro')))
print("TEST AUC-micro : {:.6f}".format(roc_auc_score(test_data_labels, test_predict, average='micro')))