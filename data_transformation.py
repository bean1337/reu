import pandas as pd
import pickle as pk

from sklearn.preprocessing import MultiLabelBinarizer
import numpy as np

def get_all_content(df):
  all_doc     = list(df['text'].apply(lambda x: " ".join(x.lower().split())))
  all_labels  = list(df['label'].apply(lambda x: x.lower().split(',')))
  return all_doc, all_labels

def get_data(data_name, train_path, test_path, label_path):

  train = pd.read_csv(train_path)
  dev   = pd.read_csv(test_path)
  with open(label_path,'r') as f:
    data = [k.strip() for k in f.readlines()]

  train_doc, train_labels = get_all_content(train)
  test_doc, test_labels   = get_all_content(dev)
  total_labels            = train_labels + test_labels

  mlb         = MultiLabelBinarizer()
  labels_data = mlb.fit_transform(mk)
  labels_data = labels_data.tolist()
  all_labels  = list(mlb.classes_)

  train_labels_one_hot = labels_data[:len(train_doc)]
  test_labels_one_hot  = labels_data[len(train_doc):]

  print(len(train_labels_one_hot) == len(train_labels))
  print(len(test_labels_one_hot) == len(test_labels))

  df_train = pd.DataFrame({'comment_text': train_doc, 'list': train_labels_one_hot})
  df_test  = pd.DataFrame({'comment_text': test_doc,  'list': test_labels_one_hot})

  df_train.to_csv(f'{data_name}_train.csv',index=False)
  df_test.to_csv(f'{data_name}_test.csv',  index=False)

  with open(f'{data_name}_labels_order.pk','wb') as f:
    pk.dump(all_labels, f)

train_path = './drive/MyDrive/train.csv'
test_path  = './drive/MyDrive/dev.csv'
label_path = './drive/MyDrive/labels.txt'

get_data('eurlex', train_path,test_path, label_path)
